# SupportXR

**Getting Started**:

**Devpost:** <https://devpost.com/software/myit>

**Submission Video:** <https://www.youtube.com/watch?v=FdJI2-vcAzA>

SupportXR is a tool that allows very interactive long distance tech support on a large variety of devices. You can start a call with a technician, who can see what you see, and assist you on repair with tools like annotations.


## Setup

### Hardware Required

- Meta Quest 3
- Unity Compatible Computer

### Software Dependencies

- Unity Game Engine version `2022.3.18f`

## Run

1. Open Unity
2. Plug Meta Quest 3 into your Computer
3. In Unity
  - Click File
  - Click Build And Run (Make sure that you are building to Android devices)
4. Open Library inside the VisionOS on the Meta Quest 3.
5. Click the name of your build

## Shout-Outs

Meta Quest Mentors: For help with the Meta Quest All In One SDK

**Special Shout-Out:** Gregory Osborne for helping us setup our XR Project in Unity for the first time. Elijah Knisley from Foundry for helping with our development with the Normcore product.